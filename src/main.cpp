/*This source code copyrighted by Lazy Foo' Productions (2004-2019)
and may not be redistributed without written permission.*/

//Using SDL, SDL_image, standard IO, and strings
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#ifdef __linux__
    #include <unistd.h>
#else
    #include <windows.h>
#endif

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* CreateTexture();

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//The window renderer
SDL_Renderer* gRenderer = NULL;

//Current displayed texture
SDL_Texture* gTexture = NULL;
void* mPixels;
int mPitch;

SDL_Surface* LoadedSurface = NULL;

//The Surface we will use to hold pixels
SDL_Surface* gSurface = NULL;

//A formatted surface we use to manipulate pixels in texture
SDL_Surface* formattedSurface = NULL;

//Main loop flag
bool quit = false;

//Event handler
SDL_Event e;


bool init()
{
    //Initialization flag
    bool success = true;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        //Set texture filtering to linear
        if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
        {
            printf( "Warning: Linear texture filtering not enabled!" );
        }

        //create a surface
        gSurface = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0, 0, 0, 0);
        Uint32 *pixels = (Uint32 *)gSurface->pixels;

        //define a pixel color
        Uint32 tColor = 0xFF00FF00;
        //get the surface pixels

        //set a pixel on the surface to our pixel color
        pixels[ (5 * SCREEN_WIDTH) + 5 ] = tColor;

        //Create window
        gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( gWindow == NULL )
        {
            printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
            success = false;
        }
        else
        {
            //Create renderer for window
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
            if( gRenderer == NULL )
            {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else
            {
                //Initialize renderer color
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if( !( IMG_Init( imgFlags ) & imgFlags ) )
                {
                    printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    //Loading success flag
    bool success = true;

    //Load PNG texture
    gTexture = CreateTexture();
    if( gTexture == NULL )
    {
        printf( "Failed to load texture image!\n" );
        success = false;
    }

    LoadedSurface = IMG_Load("../data/texture.png");

    return success;
}

void close()
{
    //Free loaded image
    SDL_DestroyTexture( gTexture );
    gTexture = NULL;

    //free surface
    SDL_FreeSurface( gSurface );
    SDL_FreeSurface(formattedSurface);

    //Destroy window
    SDL_DestroyRenderer( gRenderer );
    SDL_DestroyWindow( gWindow );
    gWindow = NULL;
    gRenderer = NULL;

    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();
}

SDL_Texture* CreateTexture()
{
    //The final texture
    SDL_Texture* newTexture = NULL;

    //build formatted surface from gSurface
    SDL_Surface* formattedSurface = SDL_ConvertSurfaceFormat( gSurface, SDL_GetWindowPixelFormat( gWindow ), 0 );
    //Create texture
    newTexture = SDL_CreateTexture( gRenderer, SDL_GetWindowPixelFormat( gWindow ), SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h );

    if( newTexture == NULL )
    {
        printf( "Unable to create texture! SDL Error: %s\n", SDL_GetError() );
    } else
    {
        //Lock texture for manipulation
        SDL_LockTexture( newTexture, NULL, &mPixels, &mPitch );

        //Copy loaded/formatted surface pixels
        memcpy( mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h );

        //Unlock texture to update
        SDL_UnlockTexture( newTexture );
        mPixels = NULL;
    }

    //Get rid of old formatted surface
    SDL_FreeSurface( formattedSurface );

    return newTexture;
}

bool lockTexture()
{
    bool success = true;

    //Texture is already locked
    if( mPixels != NULL )
    {
        printf( "Texture is already locked!\n" );
        success = false;
    }
    //Lock texture
    else
    {
        if( SDL_LockTexture( gTexture, NULL, &mPixels, &mPitch ) != 0 )
        {
            printf( "Unable to lock texture! %s\n", SDL_GetError() );
            success = false;
        }
    }

    return success;
}

bool unlockTexture()
{
    bool success = true;

    //Texture is not locked
    if( mPixels == NULL )
    {
        printf( "Texture is not locked!\n" );
        success = false;
    }
    //Unlock texture
    else
    {
        SDL_UnlockTexture( gTexture );
        mPixels = NULL;
        mPitch = 0;
    }

    return success;
}

int main( int argc, char* args[] )
{
    //Start up SDL and create window
    if( !init() )
    {
        printf( "Failed to initialize!\n" );
    }
    else
    {
        //Load media
        if( !loadMedia() )
        {
            printf( "Failed to load media!\n" );
        }
        else
        {
            Uint32 *pixels = (Uint32 *)gSurface->pixels;                //holds the surface pixels we're going to blit to
            Uint32 *Src_pixels = (Uint32 *)LoadedSurface->pixels;       //holds the surface pixels we're going to blit from

            int Wave;                                                   //value of the SinWave function for the current horizontal line
            int Seed = 0;                                               //Overall value of the SinWave Function
            double SinOffset;                                           //resultant offset that SinWave function yields, used to shift image

            //parts of our sine wave
            int Amplitude = 300;
            int WaveLength = 1;

            //
            float PI = 3.14159;

            //While application is running
            while( !quit )
            {
                //Start with the same seed every time
                Wave = Seed;

                //clear our surface
                SDL_FillRect(gSurface, NULL, 0x000000);

                //iterate top to bottom through the surface we want to blit from
                for(int img_y = 0; img_y < LoadedSurface->h; img_y++)
                {
                    //every line, increase the sine wave
                    if(img_y % WaveLength == 0)
                    {
                        Wave++;
                    }

                    //iterate left to right through surface we want to blit from
                    for(int img_x = 0; img_x < LoadedSurface->w; img_x++)
                    {

                        //calculate the offset, sin function expects value in radians so convert from degrees to radians
                        SinOffset = (sin (Wave*(PI)/180))*Amplitude;

                        //linearly go through destination surface, copying pixels from source surface in sin wave pattern
                        pixels[(img_y * LoadedSurface->w) + img_x] = Src_pixels[(img_y * LoadedSurface->w) + img_x  + (int)SinOffset];
                    }
                }

                //increase the SinWave seed every frame
                Seed++;

                //Handle events on queue
                while( SDL_PollEvent( &e ) != 0 )
                {
                    //User requests quit
                    if( e.type == SDL_QUIT )
                    {
                        quit = true;
                    }
                }

                //Clear screen
                SDL_RenderClear( gRenderer );

                //****Update the texture to be drawn on screen****
                lockTexture();
                //create a formatted surface
                formattedSurface = SDL_ConvertSurfaceFormat( gSurface, SDL_GetWindowPixelFormat( gWindow ), 0 );
                //blit to surface
                memcpy( mPixels, formattedSurface->pixels, formattedSurface->pitch * formattedSurface->h );
                unlockTexture();
                //****End Update the texture to be drawn on screen****

                //Render texture to screen
                SDL_RenderCopy( gRenderer, gTexture, NULL, NULL );

                //Update screen
                SDL_RenderPresent( gRenderer );
            }
        }
    }

    //Free resources and close SDL
    close();

    return 0;
}
